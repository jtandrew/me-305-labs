'''
@file main_encoder.py
@brief A file that runs the encoder and ui tasks simultaneously
'''


from encoder import EncoderDriver
from fsm_encoder import EncoderTask
from ui_encoder import UI
import pyb



encoder = EncoderDriver(Pin_1_Location= pyb.Pin.cpu.A6, Pin_2_Location=pyb.Pin.cpu.A7,timer=3,ppr=50,cpr=7,max_w= 1000,pulses=11200)
task1 = EncoderTask(encoder)
task2 = UI(encoder)

while True:
    task1.run()
    task2.run()
