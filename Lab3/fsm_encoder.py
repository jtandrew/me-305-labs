'''
@file fsm_encoder.py
@brief A file containing a script for the finite state machine of the encoder.
@details This encoder task runs a finite state machine that cooperates with the UI finite state machine to read values from the EnocderDriver at appropriate times.
@image html encoder_state_diagram.png width=800px
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''

import utime
import shares 

class EncoderTask:
    '''
    @brief      A class that sets up the encoder finite state machine
    @details    This class implements a finite state machine to repeatedly update the motor position
    '''
    S0_init = 0 # initial state
    S1_updating = 1 # updating state
    
    def __init__(self,encoder): 
        '''
        @brief      This constructor initializes the finite state machine and calculates how often it should run.
        '''
        self.state = self.S0_init # makes sure that the encoder starts in the initial state when called for the first time
        self.encoder = encoder # makes an attribute that adds the Encoder Driver class
        self.start_time = utime.ticks_ms() # gets the intital time
        self.next_time = utime.ticks_add(self.start_time, self.encoder.interval) # calculates the next time the finite state machine should run
        self.runs = 0 # sets the total number of runs equal to zero to start
        
    def run(self):
        '''
        @brief      This method runs the finite state machine, performing actions depending on which state the machine is in.
        '''
        self.current_time = utime.ticks_ms() #gets the current time
        if utime.ticks_diff(self.current_time,self.next_time) >= 0: # if the current time is greater than or equal to the next time we want to run this machine, run it.
            if self.state == self.S0_init: # if this is the first time the machine is being run
                self.TransitionTo(self.S1_updating)
            elif self.state == self.S1_updating: 
                self.encoder.update() #calls the update method of the EncoderDriver object
                if shares.cmd: #checks if the user has inputted a character
                    shares.response = self.cipher(shares.cmd) #sets the response to the ui_encoder based on the action performed by the cipher method
                    shares.cmd = None #resets the shared command variable back to None so that it waits for another input
    
                else:
                    pass
            self.runs += 1 #adds to the run count
            self.next_time = utime.ticks_add(self.next_time,self.encoder.interval) #sets the next time the machine should run
        else:
            pass
        
    def TransitionTo(self,NewState):
        '''
        @brief      This method transitions the state of the object.
        '''
        self.state = NewState
    
    def cipher(self,value):
        '''
        @brief      This method takes what the user inputted from the ui and performs the correct action accordingly.
        '''
        if chr(value) == 'z': #zeroes the encoder position if the user has indicated they would like to do so
            self.encoder.set_position(0)
            print('The encoder has been zeroed.') #lets the user know the appropriate task has been performed
            return 'the encoder was zeroed.'
        
        elif chr(value) == 'p': #gets the encoder position if the user has indicated they would like to do so
            self.position = self.encoder.get_position()
            print('The encoder is located at: ' + str(self.position)) #lets the user know the appropriate task has been performed
            return 'the position has been retrieved.'
        
        elif chr(value) == 'd': #gets the encoder delta if the user has indicated they would like to do so
            self.delta = self.encoder.get_delta()
            print('The encoder delta is: '+ str(self.delta)) #lets the user know the appropriate task has been performed
            return 'the encoder delta was retrieved.'
        
        