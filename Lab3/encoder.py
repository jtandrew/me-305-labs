
'''
@file encoder.py
@brief A file containing a script for the encoder. This file was set up with general parameters to allow it to apply to multiple encoders.
@details This encoder class is called by the encoder finite state machine in order to get readable motor values
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
import pyb

class EncoderDriver:
    '''
    @brief      A class that sets up the encoder
    @details    This class implements 
    
    '''
    
    def __init__(self,Pin_1_Location, Pin_2_Location,timer,ppr,cpr,max_w,pulses): # must enter location as 'py.Pin.CPU. " with Pin at the end
        '''
        @brief      Sets up the encoder driver to be configured properly using 7 parameters that define the type of motor being used.
        '''
        
        self.period = 0xffff #defines the period
        self.tim = pyb.Timer(timer) # initializes the timer based on the input parameter
        self.tim.init(prescaler=0,period=self.period)
        self.tim.channel(1,pin=Pin_1_Location,mode = pyb.Timer.ENC_AB) # sets up the first encoder channel
        self.tim.channel(2,pin=Pin_2_Location,mode = pyb.Timer.ENC_AB) # sets up the second encoder channel
        self.count = self.tim.counter() # gets the first count
        self.motor_position = 0 # starts the motor position at zero
        self.ppr = ppr # attribute that contains the ppr info
        self.cpr = cpr # attribute that contains the cpr info
        self.max_w = max_w # attribute that contains the max motor rpm
        self.pulses = pulses # attribute that contains the number of pulses
        self.interval = int(self.pulses/(self.cpr*self.ppr*self.max_w*(1/60))) # calculates how often the encoder position should update
        self.terminate_command = False
        
    def update(self):
        '''
        @brief      Updates the recorded position of the encoder by calculating the good delta
        '''
        
        self.previous_count = self.count #sets the current count value to be the previous value to free up this attribute for the next count
        self.count = self.tim.counter() # gets the current vount value
        
        if abs(self.count - self.previous_count) > self.period/2: # This algorithm sorts the deltas into good and bad deltas. If the delta is larger than half the period, we know it is bad.
            self.delta = self.count - self.previous_count #calculates the bad delta
            if self.delta > 0: # if the bad delta is positive, we know that the good delta would be the bad delta minus the period
                self.good_delta = self.delta - self.Period
            elif self.delta < 0: # if the bad delta is negative, we know that the good delta would be the bad delta plus the period
                self.good_delta = self.delta + self.Period
                
        elif abs(self.count - self.previous_count) < self.period/2: # if the delta is less than half the period, the good delta is the delta and no adjustments must be made
            self.delta = self.count -self.previous_count #calculates the delta
            self.good_delta = self.delta #sets the good delta to this delta
            
        else: # if the delta is zero, we know the encoder has not changed positions 
            self.good_delta = 0
            
        self.motor_position += self.good_delta # adds the good delta value to the total motor position
        
        
        
    def get_position(self):
        '''
        @brief      returns the current position of the motor
        '''
        return self.motor_position
    
        
        
    def set_position(self, position):
        '''
        @brief      sets the current position of the motor to the input parameter value
        '''
        self.motor_position = position
        

    def get_delta(self):
        '''
        @brief      Returns the correct delta
        '''
        return self.good_delta
        
        