'''
@file ui_encoder.py
@brief A file containing a script for the finite state machine of the user interface.
@details This finite state machine prompts the user to add input and responds accordingly. This machine is used simulataneously with the encoder finite state machine to provide accurate readings from the encoder in an understandable way to the user.
@image html encoder_state_diagram_user_interface.png width=800px
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''

import pyb
import shares
import utime



class UI:
    '''
    @brief      A class that sets up how often the finite state machine should run
    @details    This class implements a finite state machine that waits for a user input and calls the encoder finite state machine to perform the appropriate task depending on what the user is looking to do. The user can choose to either zero the encoder, get the motor position, or get the motor delta.
    '''
    S0_init = 0
    S1_cmd = 1
    S2_response = 2

    
    def __init__(self,encoder): 
        '''
        @brief      This constructor sets up the finite state machine to run at correct times
        '''
        self.state = self.S0_init
        self.uart = pyb.UART(2) # BAUD DEFAULT IS 115200, match in putty
        self.encoder = encoder #adds the same encoder object 
        self.start_time = utime.ticks_ms() # gets the initial time
        self.next_time = utime.ticks_add(self.start_time, self.encoder.interval) #calculates the next time this machine should run
        self.runs = 0
        
    def run(self):
        '''
        @brief      This method will run frequently and wait for the user to provide input. Once the user provides input, this method will write over shared variables in the shares.py file to communicate to the encoder finite state machine.
        '''
        self.current_time = utime.ticks_ms()
        if utime.ticks_diff(self.current_time, self.next_time) > 0: # if the current time is greater than or equal to the next time we want to run this machine, run it.
            
            
            if self.state == self.S0_init: # the set of tasks that should be done when the finite state machine is run for the first time
                self.TransitionTo(self.S1_cmd)
                print('''Enter a lowercase value:
                "z" to zero the encoder position
                "p" to get the current encoder position
                'd' to get the most recent encoder delta
                ''')
                
                
            elif self.state == self.S1_cmd: # waits each run for a command to be inputted
                if self.uart.any():
                    shares.cmd = self.uart.readchar()
                    self.TransitionTo(self.S2_response)
                    
                else:
                    pass
                
                
                
            elif self.state == self.S2_response: # waits for the response after a command has been issued by the user
                if shares.response:
                    print('Response: confirms that ' + shares.response)
                    print('''Ready for next input:
                "z" to zero the encoder position
                "p" to get the current encoder position
                'd' to get the most recent encoder delta
                ''')
                    self.TransitionTo(self.S1_cmd)
                    shares.response = None
                    
                else:
                    pass
                
                
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time,self.encoder.interval)
        else:
            pass
    
    def TransitionTo(self,NewState):
        '''
        @brief      This method transitions the state of the object.
        '''
        self.state = NewState