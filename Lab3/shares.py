'''
@file shares.py
@brief A file containing a shared variables that allow the ui and encoder finite state machines to communicate
@details The shared variables in this file are written over as new user inputs come in. The cmd variable stores what the user has inputted and the response variable stores the information that the user wanted by inputting a command.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''



cmd = None
response = None


"""
Code that is easy to copy/paste into the command prompt to save time on the nucleo
ampy --port COM3 put main_encoder.py main_encoder.py
ampy --port COM3 put fsm_encoder.py fsm_encoder.py
ampy --port COM3 put encoder.py encoder.py
ampy --port COM3 put ui_encoder.py ui_encoder.py
ampy --port COM3 put shares.py shares.py
execfile('main_encoder.py')
"""