'''
@file Fibonacci_Sequence.py
@brief A file containing a script for calculating any index of the fibonacci sequence.
@details Enter the index when you are prompted by the script. The program will output the desired value.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''


def fib ():
    '''
    @brief When called, this function will give you back the value of any fibonacci index. Once you call the function, you will be prompted by the script to input the desired index.
    @param user_response A string that determines whether the program should end or not. This variable is written over at the end of the script when the user responds to a question asking if they would like to determine another fibonacci value.
    '''
    user_response = 'Yes'
    print(user_response)
    while any((user_response == 'Y',user_response == 'y',user_response == 'yes',user_response == 'Yes')):
        idx = int(input('What index in the Fibonacci Sequence would you like to determine? ')) 
        print('Calculating Fibonacci number at ' 'index n = {:}.'.format(idx))
        if idx < 0:
            print('Fibonacci Sequence is not defined for negative numbers')
        elif idx == 0:
            sequence = [0]
            print("Here is the full Fibonacci Sequence up to the desired index: ")
            print(sequence)
            print("The value for index " + str(idx) + " is " +str(sequence))
        elif idx == 1:
            sequence = [0,1]
            print("Here is the full Fibonacci Sequence up to the desired index: ")
            print(sequence)
            print("The value for index " + str(idx) + " is " +str(sequence.index(idx)))
        elif idx > 1:
            sequence = [0,1] 
            i = 2
            while i <= idx:
                sequence.append(sequence[i-2]+sequence[i-1])
                i = i+1
            print("Here is the full Fibonacci Sequence up to the desired index: ")
            print(sequence)
            print("The value for index " + str(idx) + " is " +str(sequence[idx]))
        user_response = input("Would you like to determine another Fibonacci Sequence value?")
        print(user_response)
        
        
if __name__ == '__main__':
    fib()
    

    
        
        
    