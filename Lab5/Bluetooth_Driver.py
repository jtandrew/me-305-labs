'''
@file Bluetooth_Driver.py
@brief A file containing the setup for the bluetooth module that is attached to the Nucleo
@details This file initializes which UART to use, which Pin to enable and sets up several methods to call in our finite state machine. Each will cause the pin to perform an action.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''


import pyb

class BluetoothDriver: 
    
    
    def __init__(self, uartnumber, baudrate):
        '''
        @brief      Initializes the proper UART and sets the Pin into output mode
        '''
        self.uart = pyb.UART(uartnumber, baudrate)
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode =  pyb.Pin.OUT_PP) #intitializes the 'LD2' LED on the Nucleo
        
    def check(self):
        '''
        @brief      Checks if there are any characters sent from the app on the phone.
        '''
        if self.uart.any():
            return 1
        else:
            return 0
        
    def read(self):
        '''
        @brief      Reads the value that has been sent by the usr on the app.
        '''
        return self.uart.read()
    
    def write(self, freq):
        '''
        @brief      Writes the text to the app that confirms to the user that the led is blinking at the specified frequency
        '''
        print('LED is blinking at ' + str(freq) +  ' HZ.')
        self.uart.write('LED is blinking at ' + str(freq) +  ' HZ.')
        
    def turnon(self):
        '''
        @brief      Turns the LED from pinA5 on
        '''
        self.pinA5.high()
        
    def turnoff(self):
        '''
        @brief      Turns the LED from pinA5 off
        '''
        self.pinA5.low()