'''
@file UI_Task.py
@brief A file containing a script that will cause the led to blink at the appropriate times based on the input frequency.
@details This script is a FSM that uses the inputted frequency to dictate how often the led should be on for as well as when to turn on and off.
@image html blinkingled_statediagram.png width=500px
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''

import utime


class BT_FSM:
    '''
    @brief      A class that sets up the states of this process.
    @details    This class is a finite state machine that gets repeatly run by the main.py file. The main.py file runs as fast as possible, but this class controls the rate at which the LED is adjusted.
    
    '''
    
    S0_init = 0
    S1_wait = 1
    S2_blinking = 2
    
    
    def __init__(self,BluetoothDriver): 
        '''
        @brief    This constructor initializes the the bluetooth driver object and stores it as an attribute so that its methods can be called later. 
        '''
        self.state = self.S0_init 
        self.bluetooth = BluetoothDriver
        
    def run(self):
        '''
        @brief    This method is run repeatedly as the object transitions from waiting for a command, and blinking the led based on the command. 
        '''
        if self.state == self.S0_init: # if this is the first time the machine is being run
            self.TransitionTo(self.S1_wait)
            print('waiting for command now')
            
        elif self.state == self.S1_wait:
            if self.bluetooth.check():
                try:
                    self.blinkfreq = int(self.bluetooth.read())
                except:
                    self.blinkfreq = 0
                    
                if self.blinkfreq>0:
                    self.interval = int(1000/(2*self.blinkfreq)) #how often to blink based on user frequency
                    self.start_time = utime.ticks_ms()
                    self.next_time= utime.ticks_add(self.start_time, self.interval)
                    self.value= 1
                    self.bluetooth.turnon()
                    self.TransitionTo(self.S2_blinking)
                    self.bluetooth.write(self.blinkfreq)
                        
                elif self.blinkfreq == -1: # special code for keeping the Led On
                    self.bluetooth.turnon()
                        
                else:
                    self.bluetooth.turnoff()
                    pass
                
            else:
                pass
     
        elif self.state == self.S2_blinking:
            if self.bluetooth.check():
                self.TransitionTo(self.S1_wait)
            
            else:
                self.current_time = utime.ticks_ms()
                if utime.ticks_diff(self.current_time, self.next_time) > 0:
                        if self.value == 1:
                            self.value += -1
                            self.bluetooth.turnoff()
                            
                        
                        elif self.value == 0:
                            self.value += 1
                            self.bluetooth.turnon()
                            
                        else:
                            pass
                        
                        self.next_time = utime.ticks_add(self.next_time,self.interval)
            
        else:
            pass
            
        
            
    
    def TransitionTo(self,NewState):
        '''
        @brief      This method transitions the state of the object.
        '''
        self.state = NewState