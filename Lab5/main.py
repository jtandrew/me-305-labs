'''
@file main.py
@brief A file containing the script that continuous runs the finite state machine.
@details The sole purpose of this file is to initialize the UART and Baudrate to create the bluetooth object that the FSM can use.
@image html blinkingled_taskdiagram.png width=500px
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''


from Bluetooth_Driver import BluetoothDriver
from UI_Task import BT_FSM


BT = BluetoothDriver(3,9600)
task = BT_FSM(BT)

while True:
    task.run()