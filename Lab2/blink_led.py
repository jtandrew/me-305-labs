'''
@file blink_led.py
@brief A file containing a script for turning on and adjusting the brightness of an led to mimmick a function
@details The led brightnessof the physical led increases and decreases with respect to a linear step function. The brightness increases from 0 (off) to 100 (full brightness) in a single period before restarting. This process is done using a finite state machine. 
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''

import pyb
import utime

class LED:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of a real and virtual LED.
    '''
    
    S0_init = 0 # Starts the value off at zero 
    S1_on = 1 # Starts the value off at zero 
    S2_off = 2 # State where the LED is off
    S1_next_value =1 #State where the next LED brightness must be established
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5) #intitializes the 'LD2' LED on the Nucleo
    tim2 = pyb.Timer(2, freq= 20000) #intitializes timer 2 on the Nucleo
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5) #intitializes channel 1 on the Nucleo
    



    def __init__(self,period,virtual):
        '''
        @brief      Creates an LED object, which can either be virtual or real.
        '''
        self.virtual = virtual #attribute that stores a zero for 'real led' and a value of one for 'virtual led'
        self.period = period*1000 # period converted to milliseconds
        self.state = self.S0_init # starts the device by making sure the LED is off
        self.start_time = utime.ticks_ms() # time in millisecond time stamp
        self.increment = 10 # brightness increment from 0 to 100
        self.value = 0 # initial brightness
        self.interval = int(self.period*(self.increment/(100+self.increment))) # period divided by the number of different values that should be specified to transition from 0 to 100 in increments
        self.next_time_real = self.start_time  # specifies the next time that we want to run the real LED
        self.next_time_virtual = self.start_time # specifies the next time that we want to run the virtual LED
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        '''
        self.current_time = utime.ticks_ms() #current time
        
        if self.virtual == 1: ## For if the LED is virtual
            if utime.ticks_diff(self.current_time, self.next_time_virtual) >= 0: # if the current time is the time we want to run switch the value of the virtual led, then continue to the next steps 
                if self.state == self.S0_init:
                    print("Virtual LED initially set to OFF") 
                    self.state = self.S1_on
                elif self.state == self.S1_on: # if the virtual led is currently on...
                    print("Virtual LED ON") # Show it is on
                    self.state = self.S2_off # Switch the state to show the Virtual LED should be off next
                elif self.state == self.S2_off: # same as state 1 but opposite (turning from off to on)
                    print("Virtual LED OFF")
                    self.state = self.S1_on
                else:
                    print('error - virtual led') # just in case something goes wrong.
                self.next_time_virtual = utime.ticks_add(self.next_time_virtual, int(self.period/2)) # the next time we want to switch the virtual led will be half the period because it should be on for duty cycle 50-100 and off fofr 0-50
            else:
                pass     
                
                
        
        ## For if the LED is real
        elif self.virtual == 0: 
            if utime.ticks_diff(self.current_time, self.next_time_real) >= 0:
                if self.state == self.S0_init:
                    print("Real LED initially set to O")
                    self.state = self.S1_next_value
                    self.t2ch1.pulse_width_percent(self.value) # sets the value attribute to the brightness of the real LED
                elif self.state == self.S1_next_value:
                    if self.value < 100:
                        self.value += self.increment
                        print("Real LED : " + str(self.value))
                        self.t2ch1.pulse_width_percent(self.value)
                    elif self.value >= 100:
                        self.value = 0 
                        print("Real LED : " + str(self.value))
                        self.t2ch1.pulse_width_percent(self.value)
                    else:
                        pass
                else:
                    pass
                self.next_time_real = utime.ticks_add(self.next_time_real, self.interval) # we want the enxt time to be based on how many increments we want per period
            else:
                pass     
                
            
            
        else:
            pass
            
        


