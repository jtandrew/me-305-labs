'''
@file main_blink.py
@brief A file that runs two blinking led tasks. One is a virtual LED that turns on at 50+ percent duty cycle. The other is a real LED that changes brighness based off a linear step function. Input the period of the LED(in seconds) and the LED type to run the LED method.
'''

from blink_led import LED

                 
## Task objects
task1 = LED(2, 0) # 2 second period, real led
task2 = LED(2, 1)  # 2 second period, virtual led




# Runs task1.run() and task2.run() a large number of times
for N in range(50000000): 
    task1.run()
    task2.run()