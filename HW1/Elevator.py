'''
@file Elevator.py

@details This file serves as an example implementation of a finite-state-machine using Python. The example will implement some code to control an imaginary elevator that serves two floors. The elevator has a button on each floor to call the elevator. There are sensors that tell the elevator which floor the elevator is currently at. The following shows the state transition diagram for this exercise
@image html elevator_state_diagram.png width=800px
@brief A file containing a pseudo elevator with controls
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control two-story elevator.
    @details    This class implements a finite state machine to control the
                operation of a building elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_moving_down  = 1
    
    ## Constant defining State 2
    S2_floor_1 = 2
    
    ## Constant defining State 3
    S3_moving_up      = 3
    
    ## Constant defining State 4
    S4_floor_2    = 4
    
    S5_DO_NOTHING = 5
    
    def __init__(self, Motor,at_First,at_Second, Button_1, Button_2, interval):
        '''
        @brief      Creates an Elevator object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy so other functions can use them
        self.at_First = at_First
        self.at_Second = at_Second
        self.Button_1 = Button_1
        self.Button_2 = Button_2
        
        self.runs = 0
        ## The timestamp for the first iteration
        
        self.start_time = time.time() 
        ## Starting time for the power up
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_moving_down)
                self.Motor.Reverse() 
            
            elif(self.state == self.S1_moving_down):
                print(str(self.runs) + ' State 1 - Moving Downward {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                
                if self.at_First.getButtonState():
                    self.transitionTo(self.S2_floor_1)
                    self.Motor.Stop()
            
            elif(self.state == self.S2_floor_1):
                print(str(self.runs) + ' State 2 -  First Floor {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                if self.Button_2.getButtonState():
                    self.transitionTo(self.S3_moving_up)
                    self.Motor.Forward()
            
            elif(self.state == self.S3_moving_up):
                print(str(self.runs) + ' State 3 - Moving Upward {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.at_Second.getButtonState():
                    self.transitionTo(self.S4_floor_2)
                    self.Motor.Stop()
            
            elif(self.state == self.S4_floor_2):
                print(str(self.runs) + ' State 4 - Second Floor {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.Button_1.getButtonState():
                    self.transitionTo(self.S1_moving_down)
                    self.Motor.Reverse()
                    
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                user calling the elevator or be activated by a sensor. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator go up or down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward, which lifts the elevator
        '''
        print('Elevator moving up!')
    
    def Reverse(self):
        '''
        @brief Moves the motor in reverse, which lowers the elevator
        '''
        print('Elevator moving down!')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Elevator is stopped.')

