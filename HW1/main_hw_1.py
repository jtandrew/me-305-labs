'''
@file main_hw_1.py
'''

from Elevator import Button, MotorDriver, TaskElevator
        
## Motor Object
Motor1 = MotorDriver()
## Button Object for the sensor that tells us if the elevator is on the first floor
at_First1 = Button('PB6')
## Button Object for the sensor that tells us if the elevator is on the second floor
at_Second1 = Button('PB7')
## Button Objects for whether the elevator has been called on each floor
Button_1_1 = Button('PB8')
Button_2_1 = Button('PB9')
                 
## Motor Object
Motor2 = MotorDriver()
## Button Object for the sensor that tells us if the elevator is on the first floor
at_First2 = Button('PC6')
## Button Object for the sensor that tells us if the elevator is on the second floor
at_Second2 = Button('PC7')
## Button Objects for whether the elevator has been called on each floor
Button_1_2 = Button('PC8')
Button_2_2 = Button('PC9')
                 
## Task objects
task1 = TaskElevator(Motor1, at_First1, at_Second1, Button_1_1, Button_2_1, .1) 
task2 = TaskElevator(Motor2, at_First2, at_Second2, Button_1_2, Button_2_2, .1) 


# To run the task call task1.run() and task2.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
