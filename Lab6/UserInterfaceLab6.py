'''
@file UserInterfaceLab6.py
@brief A file containing a script that calls the user to imput and Kp value.
@details This script sends the Kp value to the Nucleo and generates the plots after receiving data from the Controller Task. This script acts as the front-end.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''
import serial
import matplotlib.pyplot as plt

ser = serial.Serial(port='COM5',baudrate=115273,timeout=11)  # leave a 10 second wait time


def enterCommand():
    
    '''
    @brief This function prompts the user to enter the Kp value and converts it to a larger K'p value in order for it to be sent to the Nucleo. A decimal value cannot be sent.'
    '''
    Kp = float(input('Enter the proportional constant Kp: '))
    Vm = 3.3 # Volts
    K_prime_p_large = int(Kp*1000/Vm)
    ser.write(str(K_prime_p_large).encode('ascii'))
    wait_time = 6
    return wait_time
        

def omegaData():
    '''
    @brief This function gets the data from the Controller Task and formats it properly. It also adds the data for each new Kp to its own column.
    '''
    omega = 0
    omega = ser.readall().decode('ascii')
    omega = omega.replace(' ','')
    omega = omega.replace('[','')
    omega = omega.replace(']','')
    omega_arr = omega.strip().split(',') # Makes a 2 item list with sub lists for each
    print(omega_arr)
    omega_arr_float = []
    for i in omega_arr:
        omega_arr_float.append(float(i))
    return omega_arr_float
    

def timeData(recording_time,delta_t):
    '''
    @brief This function creates the time list that will be used to generate the appropriate plots. These values are the x values.
    '''
    delta_t_list = [0]
    for i in range(int((recording_time/delta_t)-2)):
        delta_t_list.append(round(delta_t_list[i]+delta_t,2))
    return delta_t_list

def omega_ref_Data(recording_time,delta_t):
    '''
    @brief This function creates the reference omega plot. This reference omega is hardcoded in this lab, but will be set up as a csv file in the next lab.
    '''
    omega_ref_list = [50] 
    for i in range(int((recording_time/delta_t)-2)):
        omega_ref_list.append(omega_ref_list[i])
    return omega_ref_list




recording_time = 5 #seconds
delta_t = .05 #seconds
mega_list = []
mega_list.append(timeData(recording_time,delta_t))
mega_list.append(omega_ref_Data(recording_time,delta_t))
for n in range(3): 
    wait_time = enterCommand() 
    mega_list.append(omegaData())
print(mega_list)

    
    
#Create a Plot using MatPlotLib
plt.figure(1)
plt.plot(mega_list[0],mega_list[1], label = 'Reference Velocity')
plt.plot(mega_list[0],mega_list[2], label = 'Kp = .1')
plt.plot(mega_list[0],mega_list[3], label = 'Kp = .2')
plt.plot(mega_list[0],mega_list[4], label = 'Kp = .3')
plt.title('Motor Angular Velocity versus Time')
plt.xlabel('Time (seconds)')
plt.ylabel('Motor Angular Velocity (rad/s)')
plt.legend()
    
    #Create a CSV using Numpy and Pandas
    #arr = np.array([my_time_list,my_values_list])
    #formatted_array = arr.transpose()
    #pd.DataFrame(data).to_csv("D:/Fall 2020 Classes/ME 305/data.csv",header = None,index = None) # change the file location as necessary
    #plt.plot(data)
    #np.savetxt('encoder_position_data.csv', data, delimiter=",") # alternative method
    




