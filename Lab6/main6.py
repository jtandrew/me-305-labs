'''
@file Main6.py
@brief A file containing a script that constantly runs the controller task after setting up objects.
@details This file initializes the encoder, motor and controller objects and continuously runs the controller task
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''
import pyb
from MotorDriver6 import Motor
from EncoderDriver6 import Encoder
from ControllerTask6 import Controller
#import math

encoder1 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4,1000)
encoder2 = Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4,1000)

pin_nSLEEP = pyb.Pin.cpu.A15
tim3 = pyb.Timer(3,freq=20000)
    
motor1_first_pin = pyb.Pin.cpu.B4
motor1_second_pin = pyb.Pin.cpu.B5
motor1 = Motor(pin_nSLEEP, motor1_first_pin, motor1_second_pin, tim3)

motor2_first_pin = pyb.Pin.cpu.B0
motor2_second_pin = pyb.Pin.cpu.B1
motor2 = Motor(pin_nSLEEP, motor2_first_pin, motor2_second_pin, tim3)
    




## Motor Velocity and Controller Parameters
#omega_ref_rad_s = 50 # desired angular velocity rad/s
controller1 = Controller(encoder1,motor1)
controller2 = Controller(encoder2,motor2)

while True:
    #controller1.run()
    controller2.run()
    
    
    
    