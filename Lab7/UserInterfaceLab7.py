'''
@file UserInterfaceLab7.py
@brief A file containing a script that calls the user to imput a Kp value. It also generates plots and analyzes the performance characteristic value J.
@details This script sends the Kp value to the Nucleo and generates the plots after receiving data from the Controller Task. This script acts as the front-end.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''
import serial
import array
import matplotlib.pyplot as plt

ser = serial.Serial(port='COM5',baudrate=115273,timeout=16)  # leave a 10 second wait time


def enterCommand():
    
    '''
    @brief This function prompts the user to enter the Kp value and converts it to a larger K'p value in order for it to be sent to the Nucleo. A decimal value cannot be sent.'
    '''
    Kp = float(input('Enter the proportional constant Kp: '))
    Vm = 3.3 # Volts
    K_prime_p_large = int(Kp*1000/Vm)
    ser.write(str(K_prime_p_large).encode('ascii'))
        

def omegaData():
    '''
    @brief This function gets the data from the Controller Task and formats it properly. It also adds the data for each new Kp to its own column.
    '''
    both_arr = ser.readall().decode('ascii')
    split_arr = both_arr.strip().split(';') 
    omega = split_arr[0]
    position = split_arr[1]
    omega = omega.replace("array('f', " , '')
    position = position.replace("array('f', " , '')
    omega = omega.replace(')' , '')
    omega = omega.replace(' ','')
    omega = omega.replace('[','')
    omega = omega.replace(']','')
    position = position.replace(')' , '')
    position = position.replace(' ','')
    position = position.replace('[','')
    position = position.replace(']','')
    omega_arr = omega.strip().split(',') # Makes a 2 item list with sub lists for each
    position_arr = position.strip().split(',')
    omega_arr_float = []
    position_arr_float = []
    for i in omega_arr:
        omega_arr_float.append(float(i))
    for i in position_arr:
        position_arr_float.append(float(i))
    return omega_arr_float,position_arr_float
    
    


def ref_Data():
    '''
    @brief This function creates the reference time, position and velocity arrays. This reference velocity, position and time are imported from a csv in this lab.
    '''
    t_arr = array.array('d',[])
    omega_ref_arr = array.array('d',[])
    pos_ref_arr = array.array('d',[])
    
    ref = open('reference20mswithtime.csv');
    
    while True:
        # Read a line of data. It should be in the format 't,v,x\n' but when the
        # file runs out of lines the lines will return as empty strings, i.e. ''
        line = ref.readline()
    
        # If the line is empty, there are no more rows so exit the loop
        if line == '':
            break
    
        # If the line is not empty, strip special characters, split on commas, and
        # then append each value to its list.
        else:
            (t,v,x) = line.strip().split(',');
            t_arr.append(float(t))
            omega_ref_arr.append(float(v))
            pos_ref_arr.append(float(x))
        
    ref.close()
        
    return t_arr, omega_ref_arr, pos_ref_arr


def calcJ(delta_t,theta,omega,theta_ref,omega_ref):
    '''
    @brief This function calculates the J value (performance characteristics) after collecting the data and comparing it to the reference data.
    '''
    J = 0
    i = 0
    while i < (len(theta_ref)-1):
        J += ((omega_ref[i]-omega[i])**2 + (theta_ref[i]-theta[i])**2)/delta_t
        i += 1
    return J



## Script

#Establish time interval and create the reference data
delta_t = 20/1000 #seconds
[time_ref, velocity_ref, position_ref] = ref_Data()

#Remove extra data points
time_ref.pop()
time_ref.pop()
velocity_ref.pop()
velocity_ref.pop()
position_ref.pop()
position_ref.pop()

#enter Kp value
enterCommand() 

#Get all the actual angles and velocities
(velocity,position) = omegaData()
J = calcJ(delta_t,position,velocity,position_ref,velocity_ref)
print(J)

#Create a Plot using MatPlotLib
plt.figure(figsize=(10,10))
plt.subplot(211)
plt.plot(time_ref, velocity_ref, label = 'Reference Velocity')
plt.plot(time_ref, velocity, label = 'Kp=.5')
plt.legend()
plt.title('Motor Angular Velocity versus Time')
plt.xlabel('Time (seconds)')
plt.ylabel('Motor Angular Velocity (RPM)')


plt.subplot(212)
plt.plot(time_ref, position_ref,  label = 'Reference Position')
plt.plot(time_ref, position, label = 'Kp=.5')
plt.legend()
plt.title('Position versus Time')
plt.xlabel('Time (seconds)')
plt.ylabel('Position (degrees)')

    




