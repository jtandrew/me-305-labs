'''
@file ControllerTask7.py
@brief A file containing the fsm for the lab. 
@details This file takes the input Kp value that was sent to the front-end, collects data for 5 seconds, and then sends that data back to the front-end for plotting. This file acts as the controller for the system.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''

import utime
import pyb
import array

class Controller:
    '''
    @brief      A class that sets up the data collection by acting as a finite state machine.
    @details    This class is a finite state machine that gets repeatly run by the main.py file. The main.py file runs as fast as possible, but this class controls the rate at which the data points are collected based on the desired data collection rate(50 ms).
    
    '''
    
    S0_init = 0
    S1_wait = 1
    S2_collect = 2
    S3_sendData = 3
    S4_done = 4
    
    def __init__(self,encoder,motor):
        '''
        @brief    This constructor initializes the data collection class and establishes the reference omega.  
        '''
        self.state = self.S0_init # makes sure that the encoder starts in the initial state when called for the first time
        self.encoder = encoder # makes an attribute that adds the Encoder Driver class
        self.motor = motor
        self.delta_t = 20 # ms
        self.myuart = pyb.USB_VCP()
        self.omega_ref_arr = array.array('f',[])
        self.pos_ref_arr = array.array('f',[])
        self.ref = open('reference20ms.csv')
        while True:
            self.line = self.ref.readline()
            # If the line is empty, there are no more rows so exit the loop
            if self.line == '':
                break
    
            # If the line is not empty, strip special characters, split on commas, and
            # then append each value to its list.
            else:
                (v,x) = self.line.strip().split(',');
                self.omega_ref_arr.append(float(v))
                self.pos_ref_arr.append(float(x))
        
        self.ref.close()
    
    def run(self):
        
        '''
        @brief    This method is run repeatedly as the object transitions from waiting for a Kp, to collecting data and then sending the data. 
        '''
        self.current_time = utime.ticks_ms() #gets the current time
            
        if self.state == self.S0_init: # if this is the first time the machine is being run
            self.TransitionTo(self.S1_wait)
            
        elif self.state == self.S1_wait:
             if self.myuart.any():
                 self.k_prime_p_large = int(self.myuart.read().decode('ascii'))
                 self.k_prime_p = self.k_prime_p_large/1000
                 self.TransitionTo(self.S2_collect)
                 self.motor.enable() # turn on motor
                 self.start_collection_time = utime.ticks_ms()
                 self.next_time = utime.ticks_add(self.start_collection_time, self.delta_t)
                 self.runs = 0
                 self.L = 0
                 self.omega_arr = array.array('f',[])
                 self.position_arr = array.array('f',[])
             else:
                 self.encoder.update()

                
        elif self.state == self.S2_collect:
            if utime.ticks_diff(self.current_time,self.next_time) >= 0:
                if utime.ticks_diff(self.current_time,self.start_collection_time) >= 15000: # if 5 seconds has elapsed 
                    self.TransitionTo(self.S3_sendData)
                    self.motor.set_duty(0)
                    self.motor.disable()
                else:
                    self.encoder.update()
                    self.omega = (60/360)*self.encoder.get_delta()/(self.delta_t/1000) #rpm
                    self.omega_arr.append(self.omega)
                    self.position_arr.append(self.encoder.get_position())
                    self.newL = self.k_prime_p*(self.omega_ref_arr[self.runs]-self.omega_arr[self.runs])
                    self.L += self.newL
                    self.motor.set_duty(round(self.L)) #set duty cycle for motor
                    self.runs += 1
                    
                self.next_time = utime.ticks_add(self.next_time,self.delta_t)
            
            else:
                pass
            
                
        elif self.state == self.S3_sendData: 
            self.myuart.write('{:};{:}\r\n'.format(self.omega_arr,self.position_arr))
            self.TransitionTo(self.S1_wait)

            
        else:
            pass

    
    def TransitionTo(self,NewState):
        '''
        @brief      This method transitions the state of the object.
        '''
        self.state = NewState
        
       