'''
@file main.py
@brief A file containing a script that starts when the Nucleo is turned on. 
@details This file creates data collection and encoder classes and repeatedly updates the encoder and stores the data point using the appropriate methods.
@image html lab4_taskdiagram.png width=800px
@image html lab4_statediagram.png width=800px
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''
import pyb
from UI_data import DataCollection
from UI_data import EncoderDriver

freq = 5
time = 10
encoder= EncoderDriver(Pin_1_Location= pyb.Pin.cpu.A6, Pin_2_Location=pyb.Pin.cpu.A7,timer=3,ppr=50,cpr=7,max_w= 310,pulses=700)
data_collection = DataCollection(encoder,freq,time)
myuart = pyb.UART(2)

while True:
    encoder.update()
    data_collection.run()
    
       
       
        