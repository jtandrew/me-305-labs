'''
@file UI_front.py
@brief A file containing a script prompts the user to start data collection, allows the user to stop collecting at any time, and then send the data. The script will then save the data as a csv file and plot the data.
@details This script interacts with the user to allow them to collect encoder position values for a set frequency. The data is plotted to show how the motor moves with time. The data is saved as a csv in case the user needs it later.
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''
import serial
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

ser = serial.Serial(port='COM3',baudrate=115273,timeout=11)  # leave a 10 second wait time


def enterCommand():
    
    '''
    @brief This function prompts the user to enter the "G" key. If any other key is entered, the loop repeats until the user inputs a 'G'
    '''
    
    command = 0
    while command != 'G':
        command = input('Enter a capital "G": ')
        if command == 'G':
            ser.write(str(command).encode('ascii'))
        else:
            pass
        

def checkCommand():
    
    '''
    @brief This function prompts the user to enter the 'S' key to stop recording data. The user can press any other key to skip this step.
    '''
    command = input('Press capital S to stop recording at anytime. Press any other key to skip.')
    if command == 'S':
        ser.write(str(command).encode('ascii'))
    else:
        pass
    
def readCommand():
    '''
    @brief This function prompts the user to enter the 'Y' key to send the recorded data over the serial port. The data is then formatted into time and value lists that are later modified in the script
    '''
    
    command = input('Press capital Y to send the data.')
    if command == 'Y':
        ser.write(str(command).encode('ascii'))
        my_list = ser.readline().decode('ascii') 
        my_list_split = my_list.strip().split(';') # Makes a 2 item list with sub lists for each
        return my_list_split
    else:
        pass
    
    



for n in range(1): 
    enterCommand() 
    checkCommand()
    data = readCommand()
    my_time_list = data[0] # list of only the time values
    my_values_list = data[1] # list of the data points for the previous time values
    
    
    #Create a Plot using MatPlotLib
    plt.figure(1)
    plt.plot(my_time_list,my_values_list)
    plt.title('Encoder Position versus Time')
    plt.xlabel('Time (milliseconds)')
    plt.ylabel('Encoder Position')
    
    
    #Create a CSV using Numpy and Pandas
    arr = np.array([my_time_list,my_values_list])
    formatted_array = arr.transpose()
    pd.DataFrame(data).to_csv("D:/Fall 2020 Classes/ME 305/data.csv",header = None,index = None) # change the file location as necessary
    plt.plot(data)
    #np.savetxt('encoder_position_data.csv', data, delimiter=",") # alternative method
    
ser.close()




