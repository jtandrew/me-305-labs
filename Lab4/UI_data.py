'''
@file UI_data.py
@brief A file containing a script will collect data for 10 seconds and send the data to the user
@details This script is a FSM that collects 10 seconds worth of encoder data ( unless the user presses 'S' to stop data collection, and then sends the data the front-end user interface)
@image html lab4_statediagram.png width=800px
@author James Andrews
@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0

'''

import utime
import pyb


class DataCollection:
    '''
    @brief      A class that sets up the data collection by acting as a finite state machine.
    @details    This class is a finite state machine that gets repeatly run by the main.py file. The main.py file runs as fast as possible, but this class controls the rate at which the data points are collected based on the desired data collection frequency.
    
    '''
    
    S0_init = 0
    S1_wait = 1
    S2_collect = 2
    S3_sendData = 3
    
    def __init__(self,encoder,freq,time): # must enter location as 'py.Pin.CPU. " with Pin at the end
        '''
        @brief    This constructor initializes the data collection class and establishes the sampling frequency.  
        '''
        self.state = self.S0_init # makes sure that the encoder starts in the initial state when called for the first time
        self.encoder = encoder # makes an attribute that adds the Encoder Driver class
        self.freq = freq #frequency of data collection in Hz
        self.time = time*1000 #length of time it should run in seconds
        self.interval = int(1000/self.freq) #how often to update and get values (200 ms in this example)
        self.start_time = utime.ticks_ms() # gets the intital time
        self.next_time_data = utime.ticks_add(self.start_time, self.interval)
        self.runs = 0 # sets the total number of runs equal to zero to start
        self.data = []
        self.myuart = pyb.UART(2)
        self.datum_time = []
    
    def run(self):
        
        '''
        @brief    This method is run repeatedly as the object transitions from waiting for a command, to collecting data and then sending the data. 
        '''
        self.current_time = utime.ticks_ms() #gets the current time
        #self.checkState() # will terminate the program and send the data if the stoop command was entered
            
        if self.state == self.S0_init: # if this is the first time the machine is being run
            self.TransitionTo(self.S1_wait)
            #print('initialized')
            
        elif self.state == self.S1_wait:
            # if conditional on myuart.any()
            cmd = self.myuart.readchar()
            if cmd == 71:
                self.TransitionTo(self.S2_collect)
                #print('G command received')
            else:
                pass

                
        elif self.state == self.S2_collect:
            if utime.ticks_diff(self.current_time,self.next_time_data) >= 0:
                
                cmd2 = self.myuart.readchar()
                if utime.ticks_diff(self.current_time,self.datum_time[0]) >= self.time:
                    self.TransitionTo(self.S3_sendData)
                    #print('10 seconds has elapsed')
                elif cmd2 == 83:
                    self.TransitionTo(self.S3_sendData)
                    #print('emergency stop activated')
                else:    
                    self.appendNewData()
                    self.datum_time.append(utime.ticks_ms())
                    #print('new data point recorded')
                self.next_time_data = utime.ticks_add(self.next_time_data,self.interval)
            
            else:
                pass
            
                
        elif self.state == self.S3_sendData: 
            cmd3 = self.myuart.readchar()
            if cmd3 == 89:
                #print('Y command received')
                #for n in range(len(self.data)):
                self.myuart.write('{:};{:}\r\n'.format(self.datum_time,self.data))
            else:
                pass
            
        else:
            pass
       
        self.runs += 1 #adds to the run count
            
        
            
    
    def TransitionTo(self,NewState):
        '''
        @brief      This method transitions the state of the object.
        '''
        self.state = NewState
        
    def appendNewData(self):
        '''
        @brief      This method add.s a new encoder position to the data list when called
        '''
        #self.encoder.update() removed because the update is now occurring frequently
        self.new_position = self.encoder.get_position()
        self.data.append(self.new_position)
        
    def checkState(self):
        cmd = 0
        if cmd == 83: # if ascii 'S'
            self.TransitionTo(self.S2_sendData)

        else: 
            pass 
       
        
class EncoderDriver:
    '''
    @brief      A class that sets up the encoder and allows for an encoder position to be given at any point in time.
    @details    This class implements encoder information to properly understand where the motor position is while changing in position with respect to time.
    
    '''
    
    def __init__(self,Pin_1_Location, Pin_2_Location,timer,ppr,cpr,max_w,pulses): # must enter location as 'py.Pin.CPU. " with Pin at the end
        '''
        @brief      Sets up the encoder driver to be configured properly using 7 parameters that define the type of motor being used.
        '''
        
        self.period = 0xffff #defines the period
        self.tim = pyb.Timer(timer) # initializes the timer based on the input parameter
        self.tim.init(prescaler=0,period=self.period)
        self.tim.channel(1,pin=Pin_1_Location,mode = pyb.Timer.ENC_AB) # sets up the first encoder channel
        self.tim.channel(2,pin=Pin_2_Location,mode = pyb.Timer.ENC_AB) # sets up the second encoder channel
        self.count = self.tim.counter() # gets the first count
        self.motor_position = 0 # starts the motor position at zero
        self.ppr = ppr # attribute that contains the ppr info
        self.cpr = cpr # attribute that contains the cpr info
        self.max_w = max_w # attribute that contains the max motor rpm
        self.pulses = pulses # attribute that contains the number of pulses

        
    def update(self):
        '''
        @brief      Updates the recorded position of the encoder by calculating the good delta
        '''
        
        self.previous_count = self.count #sets the current count value to be the previous value to free up this attribute for the next count
        self.count = self.tim.counter() # gets the current vount value
        
        if abs(self.count - self.previous_count) > self.period/2: # This algorithm sorts the deltas into good and bad deltas. If the delta is larger than half the period, we know it is bad.
            self.delta = self.count - self.previous_count #calculates the bad delta
            if self.delta > 0: # if the bad delta is positive, we know that the good delta would be the bad delta minus the period
                self.good_delta = self.delta - self.Period
            elif self.delta < 0: # if the bad delta is negative, we know that the good delta would be the bad delta plus the period
                self.good_delta = self.delta + self.Period
                
        elif abs(self.count - self.previous_count) < self.period/2: # if the delta is less than half the period, the good delta is the delta and no adjustments must be made
            self.delta = self.count -self.previous_count #calculates the delta
            self.good_delta = self.delta #sets the good delta to this delta
            
        else: # if the delta is zero, we know the encoder has not changed positions 
            self.good_delta = 0
            
        self.motor_position += self.good_delta # adds the good delta value to the total motor position
        
        
        
    def get_position(self):
        '''
        @brief      returns the current position of the motor
        '''
        return self.motor_position
    
        
        
    def set_position(self, position):
        '''
        @brief      sets the current position of the motor to the input parameter value
        '''
        self.motor_position = position
        

    def get_delta(self):
        '''
        @brief      Returns the correct delta
        '''
        return self.good_delta